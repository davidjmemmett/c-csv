#include <stdbool.h>
#include <stdio.h>

/** The maximum length of a field in the CSV
 *  @todo make this dynamic */
#define MAX_FIELD_SIZE 255

struct CSVField {
    
    /** A pointer to the field's column */
    struct CSVColumn *column;
    
    /** A pointer to the field's row */
    struct CSVRow *row;
    
    /** A pointer to the next field in the same column */
    struct CSVField *next_field_in_column;
    
    /** A pointer to the previous field in the same column */
    struct CSVField *prev_field_in_column;
    
    /** A pointer to the next field in the same row */
    struct CSVField *next_field_in_row;
    
    /** A pointer to the previous field in the same row */
    struct CSVField *prev_field_in_row;
    
    /** A pointer to the string value of the field */
    char *value;
};

struct CSVRow {
    
    /** A pointer to the previous row */
    struct CSVRow *prev_row;
    
    /** A pointer to the next row */
    struct CSVRow *next_row;
    
    /** A pointer to the first field in the row */
    struct CSVField *first_field;
    
    /** A pointer to the last field in the row */
    struct CSVField *last_field;
};

struct CSVColumn {
    
    /** A pointer to the previous column */
    struct CSVColumn *prev_column;
    
    /** A pointer to the next column */
    struct CSVColumn *next_column;
    
    /** A pointer to the first field in the column */
    struct CSVField *first_field;
    
    /** A pointer to the last field in the column */
    struct CSVField *last_field;
};

struct CSV {
    
    /** A pointer to the first row in the CSV */
    struct CSVRow *first_row;
    
    /** A pointer to the last row in the CSV */
    struct CSVRow *last_row;
    
    /** A pointer to the first column in the CSV */
    struct CSVColumn *first_column;
    
    /** A pointer to the last column in the CSV */
    struct CSVColumn *last_column;
    
    /** A pointer to the current row being parsed */
    struct CSVRow *current_row;
    
    /** A pointer to the current column being parsed */
    struct CSVColumn *current_column;
    
    /** Whether the parser is currently inside a quoted field */
    bool in_quoted_field;
    
    /** The value buffer for the next field currently being parsed */
    char *field_buffer;
    
    /** The index of the next character to be inserted into the
     *  field_buffer by the parser */
    int field_buffer_idx;
    
    /** Whether the previously parsed character was an escape
     *  character */
    bool prev_char_is_escape;
    
    /** Whether we are currently parsing the first row of the CSV */
    bool is_first_row;
};

/** @brief Adds a new field to the specified row
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param field The field to add to the row.
 * @param row The row to add the field to.
 * @return int error code
 */
int csv_append_field_to_row(struct CSVField *field, struct CSVRow *row);

/** @brief Adds a new field to the specified column
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param field The field to add to the column.
 * @param column The column to add the field to.
 * @return error code
 */
int csv_append_field_to_column(struct CSVField *field, struct CSVColumn *column);

/** @brief Allocates memory for a new field
 * 
 * @return a pointer to the new field
 */
struct CSVField *csv_field_create();

/** @brief Appends a field to the CSV
 * 
 * Typically called during the parsing phase, this will append the field
 * to the current row and column (csv->current_row & csv->current_column)
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param field The field to add to the CSV
 * @return error code
 */
int csv_append_field(struct CSV *csv, struct CSVField *field);

/** @brief Allocates memory for a new row
 * 
 * @return a pointer to the new row
 */
struct CSVRow *csv_row_create();

/** @brief Allocates memory for a new column
 * 
 * @return a pointer to the new column
 */
struct CSVColumn *csv_column_create();

/** @brief Allocates memory for a new CSV
 * 
 * @return a pointer to the new CSV
 */
struct CSV *csv_create();

/** @brief Initialises the CSV structure
 * 
 * Creates a row and a column & allocates memory for the field_buffer
 * ready for the parsing phase
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV to initialise
 * @return error code
 */
int csv_init(struct CSV *csv);

/** @brief Clears the field_buffer used during the parsing phase
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @return error code
 */
int csv_parser_clear_field_buffer(struct CSV *csv);

/** @brief Appends a column to the end of the CSV
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param column The column to append to the CSV
 * @return error code
 */
int csv_append_column(struct CSV *csv, struct CSVColumn *column);

/** @brief Appends a row to the end of the CSV
 * 
 * Sets the current_column pointer to the first column in the CSV
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param row The row to append to the CSV
 * @return error code
 */
int csv_append_row(struct CSV *csv, struct CSVRow *row);

/** @brief Defines the behaviour of encountering a comma in the CSV
 * 
 * A comma is a delimiter, which triggers a new field in the next column
 * 
 * If the parser is currently within a quoted field, the comma is
 * appended to the field_buffer
 * 
 * After appending the field to the CSV, this sets up the CSV for the
 * next field
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @return error code
 */
int csv_parse_comma(struct CSV *csv);

/** @brief Defines the behaviour of encountering a new line (\n) in the CSV
 * 
 * A new line is a delimiter, which triggers a new field in the next row
 * 
 * If the parser is currently within a quoted field, the new line is
 * appended to the field_buffer
 * 
 * After appending the field to the CSV, this sets up the CSV for the
 * next field
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @return error code
 */
int csv_parse_newline(struct CSV *csv);

/** @brief Defines the behaviour of encountering a backslash (\) in the CSV
 * 
 * A backslash is an escape character, which will prevent the next
 * character from being parsed if it is a control character.
 * 
 * The following characters are affected by backslashes:
 *  - double quote
 *  - backslash
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @return error code
 */
int csv_parse_backslash(struct CSV *csv);

/** @brief Defines the behaviour of encountering double quotes (") in the CSV
 * 
 * Double quotes can be used to encapsulate fields within CSV files.
 * They allow certain characters to be ignored by the parser if they are
 * contained within double quotes.
 * 
 * The following characters are ignored by the parser if they are within
 * a double-quoted field:
 *  - comma
 *  - new line
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @return error code
 */
int csv_parse_dblquote(struct CSV *csv);

/** @brief Appends the character ch to the field_buffer and increments
 * the field_buffer_idx
 * 
 * This function will always append a null terminator to field_buffer in
 * case it wants to be viewed/printed/etc
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param ch The character to append to the field_buffer
 * @return error code
 */
int csv_parser_append_char_to_buffer(struct CSV *csv, char ch);

/** @brief Parses a character in the CSV
 * 
 * The following characters are control characters and will affect the
 * structure of the CSV and the behaviour of the parser:
 *  - comma (,) - @see csv_parse_comma
 *  - new line (\n) - @see csv_parse_newline
 *  - backslash (\) - @see csv_parse_backslash
 *  - double quote (") - @see csv_parse_dblquote
 * 
 * If ch is not a control character, then ch will be appended to the
 * field_buffer. @see csv_parser_append_char_to_buffer
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param ch The character to parse
 * @return error code
 */
int csv_parse_char(struct CSV *csv, char ch);

/** @brief A shortcut method to read characters from file_h, one
 * character at a time.
 * 
 * Most definitely not the most efficient method. It is advised that
 * csv_parse_char is used after iterating over chunks of a file.
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV currently being parsed
 * @param file_h An open file handle to read from
 * @return error code
 */
int csv_parse_file(struct CSV *csv, FILE *file_h);

/** @brief Sets a field's value
 * 
 * This function allocates enough memory to make a copy of the value
 * passed in.
 * 
 * Currently csv_free_row must be called to free the memory.
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param field The field to set the value on
 * @param value The new value of the field
 * @return error code
 */
int csv_field_set_value(struct CSVField *field, char *value);

/** @brief Frees heap memory allocated to the row and any fields.
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param row The row to free heap memory for
 * @return error code
 */
int csv_free_row(struct CSVRow *row);

/** @brief Frees heap memory allocated to the column.
 * 
 * This does not currently free any heap memory allocated to fields.
 * 
 * @todo add csv_free_field and do something more clever to allow
 * csv_free_column to also free the memory.
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param column The column to free heap memory for
 * @return error code
 */
int csv_free_column(struct CSVColumn *column);

/** @brief Frees heap memory allocated to the CSV, it's rows, columns &
 * fields
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV to free heap memory for
 * @return error code
 */
int csv_free(struct CSV *csv);

/** @brief Prints out the contents of the CSV by row
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV to dump
 * @return error code
 */
int csv_dump_rows(struct CSV *csv);

/** @brief Prints out the contents of the CSV by column
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param csv The CSV to dump
 * @return error code
 */
int csv_dump_columns(struct CSV *csv);

/** @brief Gets the value for the field specified by the parameters
 * row_num and column_num.
 * 
 * Both row_num and column_num start from 1 for user-friendliness.
 * 
 * @param csv The CSV to get the value from
 * @param row_num The row number to get the value from
 * @param column_num The column number to get the value from
 * @return The string value for the specified field
 */
char *csv_field_get_value(struct CSV *csv, int row_num, int column_num);

/** @brief Runs the test suite
 * 
 * Possible error codes:
 *  0 - no error
 * 
 * @param filepath The CSV file path to test against
 * @return error code
 */
int csv_test(char *filepath);
