
struct Node {
    struct Node *parent;
    struct Node *first_child;
    struct Node *last_child;
    struct Node *prev;
    struct Node *next;
    const char *name;
};

struct Node *node_create(const char *name);

int node_init(struct Node *node, const char *name);

int node_append(struct Node *parent_node, struct Node *child_node);

int node_dump(struct Node *node, int indent);

int node_destroy(struct Node *node);

int node_test();
