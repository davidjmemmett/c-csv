CC=gcc
CFLAGS=-I. -Wall

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

all: csv.o node.o main.o
	$(CC) -o csv csv.o node.o main.o

clean:
	rm -f *.o csv

test: clean all
	valgrind -s --track-origins=yes ./csv test
