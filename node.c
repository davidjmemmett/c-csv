#include "node.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct Node *node_create(const char *name) {
    struct Node *node = (struct Node *)malloc(sizeof(struct Node));
    node_init(node, name);
    return node;
};

int node_init(struct Node *node, const char *name) {
    node->name = name;
    return 0;
};

int node_destroy(struct Node *node) {
    free(node);
    return 0;
};

int node_append(struct Node *parent_node, struct Node *child_node) {
    if (parent_node->first_child == NULL) {
        parent_node->first_child = child_node;
        parent_node->last_child = child_node;
        child_node->parent = parent_node;
        return 0;
    };
    parent_node->last_child->next = child_node;
    child_node->prev = parent_node->last_child;
    child_node->next = NULL;
    parent_node->last_child = child_node;
    
    child_node->parent = parent_node;
    return 0;
};

int node_dump(struct Node *node, int indent) {
    struct Node *current_node = node;
    while (current_node != NULL) {
        for (int i = 0; i < indent; i++) {
            printf("    ");
        };
        printf(" |- Node: %s\n", current_node->name);
        
        if (current_node->first_child != NULL) {
            for (int i = 0; i < indent; i++) {
                printf("    ");
            };
            printf("    Children:\n");
            indent++;
            node_dump(current_node->first_child, indent);
            indent--;
        };
        current_node = current_node->next;
    };
    return 0;
};

int node_test() {
    printf("Adding Node 1:\n");
    struct Node *node_1 = node_create("Node 1");
    
    printf("Adding Node 1.2:\n");
    struct Node *node_1_2 =  node_create("Node 1.2");
    node_append(node_1, node_1_2);
    
    printf("Adding Node 1.2.1:\n");
    struct Node *node_1_2_1 = node_create("Node 1.2.1");
    node_append(node_1_2, node_1_2_1);
    
    printf("Adding Node 1.2.2:\n");
    struct Node *node_1_2_2 = node_create("Node 1.2.2");
    node_append(node_1_2, node_1_2_2);
    
    printf("Adding Node 1.3:\n");
    struct Node *node_1_3 = node_create("Node 1.3");
    node_append(node_1, node_1_3);
    
    node_dump(node_1, 0);
    
    node_destroy(node_1_3);
    node_destroy(node_1_2_1);
    node_destroy(node_1_2);
    node_destroy(node_1);
    return 0;
};
