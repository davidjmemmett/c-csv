#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "csv.h"

struct CSV *csv_create() {
    return calloc(1, sizeof(struct CSV));
};

struct CSVRow *csv_row_create() {
    return calloc(1, sizeof(struct CSVRow));
};

struct CSVColumn *csv_column_create() {
    return calloc(1, sizeof(struct CSVColumn));
};

struct CSVField *csv_field_create() {
    return calloc(1, sizeof(struct CSVField));
};

int csv_append_field_to_row(struct CSVField *field, struct CSVRow *row) {
    field->row = row;
    if (row->first_field == NULL) {
        row->first_field = field;
    }
    else {
        row->last_field->next_field_in_row = field;
        field->prev_field_in_row = row->last_field;
    }
    row->last_field = field;
    return 0;
};

int csv_append_field_to_column(struct CSVField *field, struct CSVColumn *column) {
    field->column = column;
    if (column->first_field == NULL) {
        column->first_field = field;
    }
    else {
        column->last_field->next_field_in_column = field;
        field->prev_field_in_column = column->last_field;
    }
    column->last_field = field;
    return 0;
};

int csv_append_field(struct CSV *csv, struct CSVField *field) {
    csv_append_field_to_row(field, csv->current_row);
    csv_append_field_to_column(field, csv->current_column);
    
    csv_field_set_value(field, csv->field_buffer);
    csv_parser_clear_field_buffer(csv);
    
    csv->in_quoted_field = false;
    return 0;
};

int csv_init(struct CSV *csv) {
    struct CSVRow *row = csv_row_create();
    struct CSVColumn *column = csv_column_create();
    
    csv->first_row = row;
    csv->last_row = row;
    csv->first_column = column;
    csv->last_column = column;
    
    csv->current_row = row;
    csv->current_column = column;
    
    csv->in_quoted_field = false;
    csv->field_buffer = calloc(1, MAX_FIELD_SIZE + 1);
    csv->field_buffer_idx = 0;
    csv->prev_char_is_escape = false;
    csv->is_first_row = true;
    return 0;
};

int csv_parser_clear_field_buffer(struct CSV *csv) {
    bzero(csv->field_buffer, MAX_FIELD_SIZE);
    csv->field_buffer_idx = 0;
    return 0;
};

int csv_append_column(struct CSV *csv, struct CSVColumn *column) {
    column->prev_column = csv->current_column;
    csv->current_column->next_column = column;
    csv->last_column = csv->current_column;
    
    csv->current_column = column;
    return 0;
};

int csv_parse_comma(struct CSV *csv) {
    if (csv->in_quoted_field) {
        csv_parser_append_char_to_buffer(csv, ',');
        return 0;
    }
    
    struct CSVField *field = csv_field_create(csv);
    csv_append_field(csv, field);
    
    if (csv->is_first_row) {
        struct CSVColumn *column = csv_column_create();
        csv_append_column(csv, column);
        return 0;
    }
    
    csv->current_column = csv->current_column->next_column;
    
    return 0;
};

int csv_append_row(struct CSV *csv, struct CSVRow *row) {
    row->prev_row = csv->current_row;
    csv->current_row->next_row = row;
    csv->last_row = csv->current_row;
    
    csv->current_row = row;
    csv->is_first_row = false;
    
    csv->current_column = csv->first_column;
    return 0;
};

int csv_parse_newline(struct CSV *csv) {
    if (csv->in_quoted_field) {
        csv_parser_append_char_to_buffer(csv, '\n');
        return 0;
    }
    
    struct CSVField *field = csv_field_create(csv);
    csv_append_field(csv, field);
    
    struct CSVRow *row = csv_row_create();
    csv_append_row(csv, row);
    
    return 0;
};

int csv_parse_backslash(struct CSV *csv) {
    if (csv->prev_char_is_escape) {
        csv->prev_char_is_escape = false;
        csv_parser_append_char_to_buffer(csv, '\\');
        return 0;
    }
    csv->prev_char_is_escape = true;
    return 0;
};

int csv_parse_dblquote(struct CSV *csv) {
    if (csv->prev_char_is_escape) {
        csv->prev_char_is_escape = false;
        csv_parser_append_char_to_buffer(csv, '\"');
        return 0;
    }
    csv->in_quoted_field = !csv->in_quoted_field;
    csv->prev_char_is_escape = false;
    return 0;
};

int csv_parser_append_char_to_buffer(struct CSV *csv, char ch) {
    csv->field_buffer[csv->field_buffer_idx] = ch;
    csv->field_buffer[csv->field_buffer_idx + 1] = '\0';
    csv->field_buffer_idx++;
    return 0;
};

int csv_parse_char(struct CSV *csv, char ch) {
    switch (ch) {
        case ',':
            return csv_parse_comma(csv);
            
        case '\n':
            return csv_parse_newline(csv);
        
        case '\\':
            return csv_parse_backslash(csv);
        
        case '\"':
            return csv_parse_dblquote(csv);
        
    };
    
    return csv_parser_append_char_to_buffer(csv, ch);
};

int csv_parse_file(struct CSV *csv, FILE *file_h) {
    
    while (!feof(file_h)) {
        char ch = fgetc(file_h);
        csv_parse_char(csv, ch);
    }
    
    return 0;
}

int csv_field_set_value(struct CSVField *field, char *value) {
    field->value = (char *)malloc(strlen(value) + 1);
    strcpy(field->value, value);
    return 0;
};

int csv_free_row(struct CSVRow *row) {
    if (row == NULL) {
        return 0;
    };
    struct CSVField *current_field = row->first_field;
    struct CSVField *next_field;
    
    while (current_field != NULL) {
        next_field = current_field->next_field_in_row;
        if (current_field->value != NULL) {
            free(current_field->value);
        }
        if (current_field != NULL) {
            free(current_field);
        }
        current_field = next_field;
    }
    free(row);
    return 0;
};

int csv_free_column(struct CSVColumn *column) {
    if (column == NULL) {
        return 1;
    };
    free(column);
    return 0;
};

int csv_free(struct CSV *csv) {
    struct CSVRow *row = csv->first_row;
    
    while (row != NULL) {
        struct CSVRow *next_row = row->next_row;
        csv_free_row(row);
        row = next_row;
    }
    
    struct CSVColumn *column = csv->first_column;
    
    while (column != NULL) {
        struct CSVColumn *next_column = column->next_column;
        csv_free_column(column);
        column = next_column;
    }
    free(csv->field_buffer);
    free(csv);
    return 0;
};

int csv_dump_rows(struct CSV *csv) {
    struct CSVRow *row = csv->first_row;
    
    int row_num = 1;
    int field_num;
    
    while (row != NULL) {
        printf(" Row %d:\n", row_num);
        struct CSVField *field = row->first_field;
        
        field_num = 1;
        
        while (field != NULL) {
            printf("  Column %d: %s\n", field_num, field->value);
            field = field->next_field_in_row;
            field_num++;
        }
        row = row->next_row;
        row_num++;
    }
    return 0;
};

int csv_dump_columns(struct CSV *csv) {
    struct CSVColumn *column = csv->first_column;
    
    int column_num = 1;
    int field_num;
    
    while (column != NULL) {
        printf(" Column %d:\n", column_num);
        struct CSVField *field = column->first_field;
        
        field_num = 1;
        
        while (field != NULL) {
            printf("  Row %d: %s\n", field_num, field->value);
            field = field->next_field_in_column;
            field_num++;
        }
        column = column->next_column;
        column_num++;
    }
    return 0;
};

char *csv_field_get_value(struct CSV *csv, int row_num, int column_num) {
    char *value = "";
    
    struct CSVRow *row = csv->first_row;
    
    int this_row_num = 1;
    int this_column_num;
    
    while (row != NULL) {
        struct CSVField *field = row->first_field;
        
        this_column_num = 1;
        
        while (field != NULL) {
            
            if (this_row_num == row_num && this_column_num == column_num) {
                return field->value;
            }
            
            field = field->next_field_in_row;
            this_column_num++;
        }
        row = row->next_row;
        this_row_num++;
    }
    
    return value;
};

/**
 * exit codes:
 *  1: invalid argument(s)
 *  2: IO error
 */
int csv_test(char *filepath) {
    printf("Parsing CSV file %s\n", filepath);
    
    struct CSV *csv = csv_create();
    csv_init(csv);
    
    FILE *file_h = fopen(filepath, "r");
    if (file_h == NULL) {
        fprintf(stderr, "Error opening file \"%s\" for reading.\n", filepath);
        return 2;
    }
    
    csv_parse_file(csv, file_h);
    
    char *value = csv_field_get_value(csv, 2, 1);
    printf("Value: %s\n", value);
    
    printf("Dump rows:\n");
    csv_dump_rows(csv);
    
    printf("Dump columns:\n");
    csv_dump_columns(csv);
    
    csv_free(csv);
    
    fclose(file_h);
    
    return 0;
}
